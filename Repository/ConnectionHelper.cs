﻿using AccessChinook.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessChinook.Repository
{
    internal class ConnectionHelper
    {
        /// <summary>
        /// Builds and returns the connection string 
        /// </summary>
        /// <returns>The connection string</returns>
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder connectStringBuilder = new SqlConnectionStringBuilder();
            connectStringBuilder.DataSource = "DESKTOP-L1J3L4L\\SQLEXPRESS";
            connectStringBuilder.InitialCatalog = "Chinook";
            connectStringBuilder.IntegratedSecurity = true;
            connectStringBuilder.TrustServerCertificate= true;
            return connectStringBuilder.ConnectionString;

        }
    }
}
