﻿using AccessChinook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessChinook.Repository
{
    public interface ICustomerRepository
    {
        public List<Customer> GetAllCustomers(); // To get all the professors 
        
        public Customer GetCustomerById(int id); // To get a single professor 

        public Customer GetCustomerByName(string Name); // To get a single professor by name
        public List<Customer> GetCustomerSubset(int limit, int offset); // To get a single professor by id
        public bool AddCustomer(Customer customer); // To add a new customer
        public bool UpdateCustomer(Customer customer); // To update an existing customer
        public List<CustomerCountry> CustomerCountryCount(); // To get the number of customers in each country
        public List<CustomerSpender> CustomerHighestSpenders(); // To get the highest spender
        public List<CustomerGenre> CustomerPopularGenre(int costumerId); // To get the most popular genre for a costumer

    }
}
