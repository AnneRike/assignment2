﻿using AccessChinook.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Numerics;
using System.Reflection;
using System.Reflection.Metadata;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AccessChinook.Repository
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Retrieves all customers from the Customer table by using SQL-query
        /// </summary>
        /// <returns>A list of Customer objects</returns>
        public List<Customer> GetAllCustomers()
        {
            List<Customer> customerList = new List<Customer>();

            // Select statement
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";

            try
            {
                using (SqlConnection connect = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connect.Open();
                    using (SqlCommand command = new SqlCommand(sql, connect))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);

                                //If one of the values is Null replace with string value "Null"
                                temp.Country = reader.IsDBNull(3) ? "Null" : reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? "Null" : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? "Null" : reader.GetString(5);

                                temp.Email = reader.GetString(6);
                                customerList.Add(temp);
                            };
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerList;
        }



        /// <summary>
        /// Retrieves a customer from the Customer table based on the provided Id by using SQL-query
        /// </summary>
        /// <param name="Id">The ID of a provided customer Id</param>
        /// <returns>A customer object</returns>
        public Customer GetCustomerById(int Id)
        {
            Customer temp = new Customer();

            // Select statement
            string sql = "select CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email From Customer where CustomerId=@CustomerId";

            try
            {
                using (SqlConnection connect = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connect.Open();
                    using (SqlCommand command = new SqlCommand(sql, connect))
                    {
                        command.Parameters.AddWithValue("@CustomerId", Id);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);

                                //If one of the values is Null replace with string value "Null"
                                temp.Country = reader.IsDBNull(3) ? "Null" : reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? "Null" : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? "Null" : reader.GetString(5);

                                temp.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return temp;
        }



        /// <summary>
        /// Retrieves a customer from the Customer table based on the provided Name by using SQL-query
        /// </summary>
        /// <param name="Name">The first name of the provided customer name</param>
        /// <returns>A customer object</returns>
        public Customer GetCustomerByName(string Name)
        {
            Customer temp = new Customer();

            // Select statement
            string sql = "select CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email From Customer Where FirstName Like @FirstName";
            try
            {
                using (SqlConnection connect = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connect.Open();
                    using (SqlCommand command = new SqlCommand(sql, connect))
                    {
                        // Matches the name given with the name of a customer from the table 
                        command.Parameters.AddWithValue("@FirstName", $"%{Name}%");

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);

                                //If one of the values is Null replace with string value "Null"
                                temp.Country = reader.IsDBNull(3) ? "Null" : reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? "Null" : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? "Null" : reader.GetString(5);

                                temp.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return temp; 
        }



        /// <summary>
        /// Retrieves a subset of customers from the Customer-table starting at the offset value. 
        /// </summary>
        /// <param name="limit">Amount of customers to retrieve</param>
        /// <param name="offset">The table row that the retrieval starts at (offset value)</param>
        /// <returns>A list of customer object that contains the subset of customers retrieved from the table</returns>
        public List<Customer> GetCustomerSubset(int limit, int offset)
        {
            List<Customer> customerList = new List<Customer>();

            // Select statement
            // Retrieves a specific range of rows from the table, starting from the offset value and fetching the
            // next limit number of rows. 
            string sql = "select CustomerId, FirstName, LastName, Country, PostalCode, Phone, " +
                "Email From Customer ORDER BY CustomerId OFFSET @offset ROWS FETCH NEXT @limit " +
                "ROWS Only;";

            try
            {
                using (SqlConnection connect = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connect.Open();
                    using (SqlCommand command = new SqlCommand(sql, connect))
                    {
                        command.Parameters.AddWithValue("@limit", limit);
                        command.Parameters.AddWithValue("@offset", offset);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);

                                //If one of the values is Null replace with string value "Null"
                                temp.Country = reader.IsDBNull(3) ? "Null" : reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? "Null" : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? "Null" : reader.GetString(5);

                                temp.Email = reader.GetString(6);
                                customerList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerList;
        }



        /// <summary>
        /// Adds a new customer to the "Customer" table 
        /// </summary>
        /// <param name="customer">The customer object to add</param>
        /// <returns>A boolean indicating whether the insertion was successful or not</returns>
        public bool AddCustomer(Customer customer)
        {
            bool success = false;
            string sql = "Insert into Customer(FirstName, LastName, Country, PostalCode, Phone, Email)" +
            "Values(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            try
            {
                using (SqlConnection connect = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connect.Open();
                    using (SqlCommand command = new SqlCommand(sql, connect))
                    {


                        //command.Parameters.AddWithValue("@CuID", professor.ID);
                        command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        command.Parameters.AddWithValue("@LastName", customer.LastName);

                        // Making it possible to add a person with a null value  
                        command.Parameters.AddWithValue("@Country", customer.Country == null ? "Null" : customer.Country);
                        command.Parameters.AddWithValue("@PostalCode", customer.PostalCode == null ? "Null" : customer.PostalCode);
                        command.Parameters.AddWithValue("@Phone", customer.Phone == null ? "Null" : customer.Phone);

                        command.Parameters.AddWithValue("@Email", customer.Email);

                        // ExecuteNonQuery() if there is a change in the database
                        success = command.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return success;
        }



        /// <summary>
        /// Updates an existing customer in the "Customer" table
        /// </summary>
        /// <param name="customer">The customer object to update based on CustomerId</param>
        /// <returns>A boolean indicating whether the update was successful or not</returns>
        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            string sql = "update Customer set FirstName = @FirstName," +
                "LastName=@LastName, Country=@Country, PostalCode=@PostalCode," +
                "Phone=@Phone, Email=@Email where CustomerId=@CustomerId"; 
            try
            {
                using (SqlConnection connect = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connect.Open();
                    using (SqlCommand command = new SqlCommand(sql, connect))
                    {


                        command.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        command.Parameters.AddWithValue("@LastName", customer.LastName);

                        // Making it possible to add a person with a null value  
                        command.Parameters.AddWithValue("@Country", customer.Country == null ? "Null" : customer.Country);
                        command.Parameters.AddWithValue("@PostalCode", customer.PostalCode == null ? "Null" : customer.PostalCode);
                        command.Parameters.AddWithValue("@Phone", customer.Phone == null ? "Null" : customer.Phone);

                        command.Parameters.AddWithValue("@Email", customer.Email);

                        // ExecuteNonQuery() if there is a change in the database
                        success = command.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return success;
        }



        /// <summary>
        /// Returns a list with the number of customers in each country by most users in descending order
        /// </summary>
        /// <returns>A list with numbers of customers in each country</returns>
        public List<CustomerCountry> CustomerCountryCount()
        {
            List<CustomerCountry> listCustomers = new List<CustomerCountry>();

            // Select statement
            string sql = "SELECT Country, COUNT(CustomerId) AS totalCust FROM Customer GROUP BY Country ORDER BY totalCust DESC;";

            try
            {
                using (SqlConnection connect = new SqlConnection(ConnectionHelper.GetConnectionString()))
                using (SqlCommand command = new SqlCommand(sql, connect))
                {
                    connect.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    { 
                        // Reads the results of the query
                        while (reader.Read())
                        {
                            CustomerCountry customerCount = new CustomerCountry();

                            customerCount.CountryName = reader.GetString(0);
                            customerCount.CustomerCount = reader.GetInt32(1);

                            listCustomers.Add(customerCount);
                        }
                        
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return listCustomers;
        }



        /// <summary>
        /// Returns the costumers and amount spent in descending order by the amount they have spent. 
        /// </summary>
        /// <returns>A list of costumer spender objects (Customer FirstName and amount spent)</returns>
        public List<CustomerSpender> CustomerHighestSpenders()
        {
            List<CustomerSpender> spenderList = new List<CustomerSpender>();

            // Select statement
            // Connects the Invoice and Costumer by using INNER JOIN tables and groups the results by custumer
            // and total spending. Orders by total spending in descending order 
            string sql = "SELECT Customer.FirstName, Invoice.Total " +
                "FROM Invoice INNER JOIN Customer " +
                "ON Invoice.InvoiceId = Customer.CustomerId GROUP BY Customer.FirstName, Invoice.Total " +
                "ORDER BY Invoice.Total DESC;";
            try
            {
                using (SqlConnection connect = new SqlConnection(ConnectionHelper.GetConnectionString()))
                using (SqlCommand command = new SqlCommand(sql, connect))
                {
                    connect.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CustomerSpender CustomerTotalSpent = new CustomerSpender();

                            CustomerTotalSpent.CustomerName = reader.GetString(0);
                            CustomerTotalSpent.CustomerTotalSpent = reader.GetDecimal(1);

                            spenderList.Add(CustomerTotalSpent);
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return spenderList;
        }



        /// <summary>
        /// Retrieves the most popular genre for a specific customer. 
        /// </summary>
        /// <param name="customerId">The Id of the customer to get the most popular genre from</param>
        /// <returns>A list of customer genre objects</returns>
        public List<CustomerGenre> CustomerPopularGenre(int customerId) 
        {
            List<CustomerGenre> customerGenreList = new List<CustomerGenre>();

            // Select statement
            // Retrieves the top 1 Genre with ties
            // Connects the Customer, Invoice, InvoiceLine, Track and Genre tables
            // Groups the results by customer name, genre name and counts the number of each genre for the customer
            string sql = "SELECT TOP 1 WITH TIES Customer.FirstName, Genre.Name, COUNT(Genre.Name) FROM Customer " +
               "INNER JOIN Invoice ON Invoice.CustomerId = Customer.CustomerId " +
                "INNER JOIN InvoiceLine ON InvoiceLine.InvoiceId = Invoice.InvoiceId " +
                "INNER JOIN Track ON Track.TrackId = InvoiceLine.TrackId " +
                "INNER JOIN Genre ON Track.GenreId = Genre.GenreId " +
                "WHERE Customer.CustomerId = @CustomerId " +
                "GROUP BY Customer.FirstName, Genre.Name " +
                "ORDER BY COUNT(Genre.Name) DESC;";

            try
            {
                using (SqlConnection connect = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    using (SqlCommand command = new SqlCommand(sql, connect))
                    {
                        connect.Open();

                        command.Parameters.AddWithValue("@customerId", customerId); 

                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                CustomerGenre CountGenreName = new CustomerGenre();

                                CountGenreName.CustomerName = reader.GetString(0);
                                CountGenreName.GenreName = reader.GetString(1); 
                                CountGenreName.CountGenreName = reader.GetInt32(2); 

                                customerGenreList.Add(CountGenreName);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerGenreList;
        }
    }
}
