README

Introduction
This repository was created as a part of an assignment for a C# course.
The assignment was split in 2 parts: Appendix A and Appendix B.
Folders and files for both assigments can be found here.

Getting started:
To use this repository you can start by cloning it using git clone with either HTTPS or SSH.

Appendix A:
Appendix A contains SQL scripts for creating a
database called SuperheroesDb. The script includes instructions for creating several
tables (Superhero, Assistant, and Power), setting up primary keys, adding relationships
between tables, populating the tables with data, and updating and deleting data.

Appendix B:
Appendix B contains files for a C# console application that interacts with the Chinook database,
which models the iTunes database of customers purchasing songs.
The application provides various functionality for reading, adding,
updating, and querying customer data, including displaying customer
information, searching for customers by name or Id, paginating customer data,
and finding statistics such as the number of customers in each country,
the highest spenders, and the most popular genre for a given customer.
The necessary classes for different data structures are located in a Models folder.

Contributors
Jacob Tornes and Anne Jacobsen Rike
