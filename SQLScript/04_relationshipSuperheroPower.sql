use SuperheroesDb1;

CREATE TABLE Superhero_Power(
SuperheroId int FOREIGN KEY REFERENCES Superhero(ID),
PowerId int FOREIGN KEY REFERENCES Power(ID),
PRIMARY KEY(SuperheroId, PowerId)
); 
