use SuperheroesDb1;

create table Superhero(
ID int Not null identity(1,1) primary key, 
Name nvarchar(50) null,
Alias nvarchar(50) null,
Origin nvarchar(50) null
);


create table Assistant(
ID int not null identity(1,1) primary key, 
Name nvarchar(50) null
);


create table Power(
ID int not null identity(1,1) primary key, 
Name nvarchar(50) null, 
Description nvarchar(255) null
);