use SuperheroesDb1;

INSERT INTO Power (Name, Description)
VALUES	('SuperStrenght', 'Makes the heroes strong.'),
		('WallCrawling', 'Ability to crawl walls and buildings.'),
		('FightingSkills', 'Can beat enemies with only fists and legs.'),
		('FlyingPower', 'Ability to fly like a bird');