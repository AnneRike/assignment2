use SuperheroesDb1 
go

ALTER TABLE Assistant
ADD SuperheroId int not null 
go 
ALTER TABLE Assistant
ADD constraint FK_SuperheroID 
FOREIGN KEY (SuperheroId) REFERENCES Superhero(ID)
on update cascade 
on delete no action 
go 
