use SuperheroesDb1;

INSERT INTO Superhero (Name, Alias, Origin)
VALUES	('Clark Kent', 'Superman', 'Krypton'),
		('Peter Parker', 'Spiderman', 'New York'),
		('Bruce Wayne', 'Batman', 'Gotham');
