﻿using AccessChinook.Repository;
using AccessChinook.Models;
using Microsoft.IdentityModel.Tokens;
using System.Xml;
using System.Diagnostics.Metrics;
using System.Numerics;

namespace AccessChinook
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ICustomerRepository repository = new CustomerRepository();

            DisplayRecords(repository);
            DisplayById(repository);
            DisplayByName(repository);
            DisplayOneCustomer(new Customer(3, "François", "Tremblay", "Canada", "H2G 1A7", "+1 (514) 721-4711", "ftremblay@gmail.com"));
            DisplayBySubset(repository);
            AddNewCustomer(repository);
            UpdateExistingCustomer(repository);


            //Displays the number of customers in each country by descending order
            List<CustomerCountry> countryCount = repository.CustomerCountryCount();
            foreach (var num in countryCount) { Console.WriteLine(num.CountryName + ": " + num.CustomerCount); }

            // Displays the highest spenders by descending order 
            List<CustomerSpender> CustomerTotalSpent = repository.CustomerHighestSpenders();
            foreach (var num in CustomerTotalSpent) { Console.WriteLine(num.CustomerName + ": " + num.CustomerTotalSpent); }

            // Displays the most popular genre for a specific costumer 
            List<CustomerGenre> CountGenreName = repository.CustomerPopularGenre(2); //edit number here - CostumerId
            foreach (var num in CountGenreName) { Console.WriteLine(num.CustomerName + " - " + num.GenreName + " - " + num.CountGenreName); }
        }

        static void DisplayRecords(ICustomerRepository repository)
        {
            DisplayAllCustomers(repository.GetAllCustomers());
        }


        static void DisplayOneCustomer(Customer customer)
        {
            // Displaying one customer
            Console.WriteLine(customer.CustomerId + " " + customer.FirstName + " " + customer.LastName + " "
                + customer.Country + " " + customer.PostalCode + " " + customer.Phone + " " + customer.Email);
        }


        static void DisplayAllCustomers(List<Customer> customerList)
        {
            // Displaying all customers by looping through customerlist and calling on DisplayOneCustomer-method
            foreach (Customer customer in customerList)
            {
                DisplayOneCustomer(customer);
            }
        }


        static void DisplayById(ICustomerRepository repository)
        {
            // Displaying customer by CustomerId by using DisplayOneCustomer-method
            DisplayOneCustomer(repository.GetCustomerById(5));  
                
        }

        static void DisplayByName(ICustomerRepository repository)
        {
            // Displaying customer by FirstName by using DisplayOneCustomer-method

            DisplayOneCustomer(repository.GetCustomerByName("Aaron"));  
        }


        static void DisplayBySubset(ICustomerRepository repository)
        {
            // Displaying page of customers from the database by using DisplayAllCustomers-method 
            DisplayAllCustomers(repository.GetCustomerSubset(10, 5));
        }

        static void AddNewCustomer(ICustomerRepository repository)
        {
            // Adds a new customer 
            Customer customer1 = new Customer()
            {
                FirstName = "Jacob",
                LastName = "Sommer",
                Country = "Jamaica",
                PostalCode = "12345",
                Phone = "12345",
                Email = "hei@gmail.com"
            };

            // Checking if the adding was successful
            if (repository.AddCustomer(customer1)) {

                Console.WriteLine("Successfully inserted a customer! congratz");
                DisplayOneCustomer(repository.GetCustomerByName("Jacob"));
            }
            else
            {
                Console.WriteLine("Unsuccessful");
            }
        }


        static void UpdateExistingCustomer(ICustomerRepository repository)
        {
            // Updating existing customer
            Customer update = new Customer()
            {
                CustomerId = 2,
                FirstName = "Truls",
                LastName = "Pedersen",
                Email = "Trulspedersen@gmail.com"
            };

            // Checking if the update was successful
            if (repository.UpdateCustomer(update))
            {

                Console.WriteLine("Successfully updated a customer! congratz");
                DisplayOneCustomer(repository.GetCustomerById(2));
            }
            else
            {
                Console.WriteLine("Unsuccessful");
            }
        }
    }
}