﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessChinook.Models
{
    public class CustomerSpender
    {
        public string CustomerName { get; set; }
        public decimal CustomerTotalSpent { get; set; }
    }
}
