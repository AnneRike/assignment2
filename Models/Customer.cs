﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessChinook.Models
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public Customer(int CustomerId, string FirstName, string LastName, string Country, string PostalCode, string Phone, string Email) 
        {
            this.CustomerId = CustomerId;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Country = Country;
            this.PostalCode = PostalCode;
            this.Phone = Phone;
            this.Email = Email;
        }

        public Customer() { }

    }
}
