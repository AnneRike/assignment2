﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessChinook.Models
{
    public class CustomerGenre
    {
        public string CustomerName { get; set; }
        public string GenreName { get; set; }
        public int CountGenreName { get; set; }
    }
}
