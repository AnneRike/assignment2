﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessChinook.Models
{
    public class CustomerCountry
    {
        public string CountryName { get; set; }
        public int CustomerCount { get; set; }
    }
}
